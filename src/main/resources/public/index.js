'use strict';
window.onload = function () {
    const vm = new Vue({
        el: '#news',
        data: {
            editMode: /edit/.test(window.location.href),
            hoverMode: false,
            subscribedScopes: [],
            availableScopes: [],
            newsItems: [],
            newsItem: {
                newsText: '',
                days: '',
                hours: '',
                minutes: '',
                scopes: [],
            },
            newScope: ''
        },
        created() {
            setInterval(this.retrieveNews.bind(this), 2000);
            // retrieve values without overwriting subscribed ones
            // setInterval(this.retrieveScopes.bind(this), 1000 * 3600);
            this.retrieveNews();
            this.retrieveScopes();
            console.log(this.editMode);
        },
        methods: {
            addNewsItem() {
                axios.post('/news', {
                    newsText: this.newsItem.newsText,
                    expirationDate: this.calculateNewEntryDuration(),
                    scopes: this.newsItem.scopes
                })
                    .then(response => {
                        this.newsItems.push(this.newsItem);
                        this.editMode = false;
                        this.updateAnimationSpeed();
                    })
                    .catch(error => console.log(error)
                    );
            },
            addNewScope() {
                axios.post('/news/scopes',
                    this.newScope, {headers: {'Content-Type': 'text/plain'}}
                )
                    .then(response => {
                        this.retrieveScopes();
                    })
                    .catch(error => console.log(error)
                    );
            },
            retrieveNews() {
                axios.get('/news', {
                    params: {
                        'scopes': this.subscribedScopes
                    }
                })
                    .then(response => {
                        this.newsItems = response.data;
                        this.updateAnimationSpeed();
                    })
                    .catch(error => console.log(error)
                    );
            },
            retrieveScopes() {
                axios.get('/news/scopes')
                    .then(response => {
                        this.availableScopes = response.data;
                    })
                    .catch(error => console.log(error)
                    );
            },
            calculateNewEntryDuration() {
                let duration = Date.now();
                if (this.newsItem.days) {
                    duration += 86400000 * this.newsItem.days;
                }
                if (this.newsItem.hours) {
                    duration += 3600000 * this.newsItem.hours;

                }
                if (this.newsItem.minutes) {
                    duration += 60000 * this.newsItem.minutes;
                }
                return duration;
            },
            updateAnimationSpeed() {
                let width = 0;
                document.querySelectorAll('.marquee div span')
                    .forEach(span => {
                        width += span.offsetWidth;
                    });
                let marqueeDiv = document.querySelector('.marquee div');
                // width += parseInt(getComputedStyle(marqueeDiv).paddingLeft) +
                // parseInt(getComputedStyle(marqueeDiv).paddingRight);
                marqueeDiv.style.animationDuration = width / 100 + "s";
            },
            cancel() {
                console.log('cancel');
                history.replaceState('', 'TOPnews', '/');
                window.location.href = "";
            }
        }
    });
};