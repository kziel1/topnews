package com.topdesk.topnews.model;

import lombok.*;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by krzysztofz on 13.04.18.
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewsItem {
    @NonNull
    private String newsText;
    @NonNull
    private Timestamp expirationDate;
    @NonNull
    private List<String> scopes;
}
