package com.topdesk.topnews.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class NewsDatabase {
    private List<NewsItem> newsItems = new ArrayList<>();
    private List<String> scopes = new ArrayList<>();
}
