package com.topdesk.topnews;

import com.topdesk.topnews.model.NewsItem;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by krzysztofz on 13.04.18.
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/news")
public class TopnewsController {

    private final NewsAccess newsAccess;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public List<NewsItem> getNews(
            @RequestParam(value = "scopes[]", required = false, defaultValue = "")
                    List<String> scopes) {
        if (scopes.isEmpty()) {
            return newsAccess.getNewsItems();
        }
//        cleanNewsDatabase();
        return newsAccess.getNewsItems().stream()
                .filter(newsItem -> CollectionUtils.containsAny(scopes, newsItem.getScopes()))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/scopes", method = RequestMethod.GET)
    public List<String> getScopes() {
        return newsAccess.getScopes();
    }

//    @RequestMapping(value = "/edit")
//    public String add() {
//        return "index";
//    }

    private void cleanNewsDatabase() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        List<NewsItem> newsItems = newsAccess.getNewsItems();
        for (int i = 0; i < newsItems.size(); i++) {
            if (now.getTime() > newsItems.get(i).getExpirationDate().getTime()) {
                newsItems.remove(i);
            }
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> postNews(@RequestBody NewsItem newsItem) {
        newsAccess.addNewsItem(newsItem);

        return null;
    }

    @RequestMapping(value = "/scopes", method = RequestMethod.POST)
    ResponseEntity<?> postScope(@RequestBody String scope) {
        if (scope == null) {
            return null;
        }
        newsAccess.addScope(scope);
        return null;
    }
}