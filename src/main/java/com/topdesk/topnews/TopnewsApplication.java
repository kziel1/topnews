package com.topdesk.topnews;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.topdesk.topnews.model.NewsDatabase;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@SpringBootApplication
public class TopnewsApplication {
    public static void main(String[] args) {
        SpringApplication.run(TopnewsApplication.class, args);
    }
}
