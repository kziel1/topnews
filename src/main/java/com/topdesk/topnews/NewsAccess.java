package com.topdesk.topnews;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.topdesk.topnews.model.NewsDatabase;
import com.topdesk.topnews.model.NewsItem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by krzysztofz on 13.04.18.
 */
@Component
@RequiredArgsConstructor
public class NewsAccess {
    private final ObjectMapper objectMapper;
    private NewsDatabase newsDatabase;

    public void addNewsItem(NewsItem newsItem) {
        newsDatabase.getNewsItems().add(0, newsItem);
        persistData();
    }

    public void addScope(String scope) {
        if (!newsDatabase.getScopes().contains(scope)) {
            newsDatabase.getScopes().add(scope);
        }
        persistData();
    }

    public List<NewsItem> getNewsItems() {
        return newsDatabase.getNewsItems();
    }

    public List<String> getScopes() {
        return newsDatabase.getScopes();
    }

    private void persistData() {
        try {
            FileWriter fileWriter = null;
            BufferedWriter bufferedWriter = null;
            String json = null;
            json = objectMapper.writeValueAsString(newsDatabase);
            fileWriter = new FileWriter("topnews-database.txt", false);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(json);
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostConstruct
    public void initialize() {
        try {
            File dataFile = new File("topnews-database.txt");
            if (dataFile.exists()) {
                try {
                    newsDatabase = objectMapper.readValue(dataFile, NewsDatabase.class);
                } catch (Exception e) {
                    FileWriter fileWriter = new FileWriter(dataFile);
                    fileWriter.write("");
                    fileWriter.close();
                }
            } else {
                dataFile.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
